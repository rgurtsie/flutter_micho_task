part of 'task_bloc.dart';

class TaskMainState {

  final List<Model> list_1;
  final List<Model> list_2;

  TaskMainState({this.list_1 = const <Model>[], this.list_2 = const <Model>[]});
}
