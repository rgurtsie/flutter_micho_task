import 'package:bloc/bloc.dart';
import 'package:collection/collection.dart';
import '../model/model.dart';
import '../repo/repository.dart';

part 'task_event.dart';
part 'task_state.dart';

class TaskBloc extends Bloc<TaskEvent, TaskMainState> {
  Repo_Task _repo_task;

  TaskBloc(this._repo_task) : super(TaskMainState()) {
    on<TaskLoadedEvent>(_onTaskLoadingEvent);
    on<TaskAddidEvent>(_onTaskAddListTitleEvent);
  }

  _onTaskAddListTitleEvent(TaskAddidEvent event, Emitter emit) {
    _repo_task.list_1.firstWhereOrNull((element) => element.id == event.id)?.flavor = event.value;
    _repo_task.list_2.firstWhereOrNull((element) => element.id == event.id)?.flavor = event.value;
    

    emit(TaskMainState(list_1: _repo_task.list_1, list_2: _repo_task.list_2));
  }

  _onTaskLoadingEvent(TaskLoadedEvent event, Emitter emit) async {
    final listData = _repo_task.list_1;
    final listData2 = _repo_task.list_2;
    await Future.delayed(Duration(seconds: 2));
    emit(TaskMainState(list_1: listData, list_2: listData2));
  }
}
