part of 'task_bloc.dart';

abstract class TaskEvent {
  const TaskEvent();
}

class TaskLoadedEvent extends TaskEvent {}

class TaskAddidEvent extends TaskEvent {
  final int id;
  final bool value;

  TaskAddidEvent({required this.id, required this.value});
}
