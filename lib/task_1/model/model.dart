class Model {
  final String title;
  final int id;
  bool flavor;

  Model({required this.title, required this.id, this.flavor = false});
}
