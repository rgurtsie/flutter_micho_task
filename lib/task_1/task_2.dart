import 'package:flutter/material.dart';

import 'package:flutter_bloc/flutter_bloc.dart';

import 'bloc/task_bloc.dart';

import 'model/model.dart';
import 'repo/repository.dart';

class My_Task_2 extends StatelessWidget {
  const My_Task_2({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MultiRepositoryProvider(providers: [
        RepositoryProvider(create: (context) => Repo_Task()),
      ], child: Task_2()),
    );
  }
}

class Task_2 extends StatelessWidget {
  const Task_2({super.key});

  @override
  Widget build(BuildContext context) {
    final repository = RepositoryProvider.of<Repo_Task>(context);
    return MultiBlocProvider(
      providers: [
        BlocProvider<TaskBloc>(
            create: (context) => TaskBloc(repository)..add(TaskLoadedEvent()))
      ],
      child: Scaffold(body: BlocBuilder<TaskBloc, TaskMainState>(
        builder: (context, state) {
          return Column(
            children: [
              Container(
                margin:
                    const EdgeInsets.only(left: 8.0, top: 50.0, bottom: 30.0),
                child: Text('Задача',
                    style: TextStyle(fontSize: 30, color: Colors.black)),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 20, bottom: 10),
                    child: Text('новинки',
                        style: TextStyle(fontSize: 20, color: Colors.black)),
                  ),
                  SizedBox(height: 100, child: widget_list(state.list_1)),
                ],
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 20, bottom: 10),
                    child: Text('рекомендаций',
                        style: TextStyle(fontSize: 20, color: Colors.black)),
                  ),
                  SizedBox(height: 100, child: widget_list(state.list_2)),
                ],
              )
            ],
          );
        },
      )),
    );
  }

  Widget widget_list(List<Model> list) {
    if (list.isNotEmpty) {
      return ListView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: list.length,
          itemBuilder: (context, index) {
            return Container(
                margin: EdgeInsets.only(left: 10.0),
                height: 100,
                width: 100,
                decoration: BoxDecoration(
                    color: Colors.amber,
                    borderRadius: BorderRadius.circular(25)),
                child: Column(children: <Widget>[
                  Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Container(),
                        IconButton(
                            onPressed: () {
                              context.read<TaskBloc>().add(TaskAddidEvent(
                                  id: list[index].id,
                                  value: !list[index].flavor));
                            },
                            icon: Icon(
                              Icons.favorite,
                              color: list[index].flavor
                                  ? Colors.blue
                                  : Colors.black,
                            ))
                      ]),
                  Center(
                    child: Text(list[index].title),
                  )
                ]));
          });
    } else if (list.isEmpty) {
      return Center(child: CircularProgressIndicator());
    }
    return Center();
  }
}
