part of 'bloc_pattern_bloc.dart';

class BlocPatternState extends Equatable {
  final List<Task> allTasks;

  BlocPatternState({ this.allTasks=const <Task>[]});

  @override
  List<Object> get props => [allTasks];
}
