part of 'bloc_pattern_bloc.dart';

abstract class BlocPatternEvent extends Equatable {
  const BlocPatternEvent();

  @override
  List<Object> get props => [];
}

class AddTaskEvent extends BlocPatternEvent {
  final Task task;

  AddTaskEvent({required this.task});



  @override
  List<Object> get props => [task];
}

class UpdateTaskEvent extends BlocPatternEvent {
  final Task task;

  UpdateTaskEvent({required this.task});



  @override
  List<Object> get props => [task];
}

class RemoveTaskEvent extends BlocPatternEvent {
  final Task task;

  RemoveTaskEvent({required this.task});



  @override
  List<Object> get props => [task];
}
