import 'package:bloc/bloc.dart';

import 'package:bloc_micho/test/models/task.dart';
import 'package:equatable/equatable.dart';

part 'bloc_pattern_event.dart';
part 'bloc_pattern_state.dart';

class BlocPatternBloc extends Bloc<BlocPatternEvent, BlocPatternState> {
  BlocPatternBloc() : super(BlocPatternState()) {
    on<AddTaskEvent>(_onAddTaskEvent);
    on<UpdateTaskEvent>(_onUpdateTaskEvent);
    on<RemoveTaskEvent>(_onRemoveTaskEvent);
  }
  _onAddTaskEvent(AddTaskEvent event, Emitter emit) {
    emit(
        BlocPatternState(allTasks: List.from(state.allTasks)..add(event.task)));
  }

  _onRemoveTaskEvent(RemoveTaskEvent event, Emitter emit) {
    emit(BlocPatternState(
        allTasks: List.from(state.allTasks)..remove(event.task)));
  }

  _onUpdateTaskEvent(UpdateTaskEvent event, Emitter emit) {
    final task = event.task;
    final int index = state.allTasks.indexOf(task);
    print("index: " + index.toString());
    List<Task> allTasks = List.from(state.allTasks)..remove(task);

    print("allTasks: ");

    task.isDone == false
        ? allTasks.insert(index, task.copyWith(isDone: true))
        : allTasks.insert(index, task.copyWith(isDone: false));
    emit(BlocPatternState(allTasks: allTasks));
  }
}
