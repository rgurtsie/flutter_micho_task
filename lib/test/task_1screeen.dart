import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'bloc/bloc_pattern_bloc.dart';
import 'models/task.dart';

void main() {
  runApp(MyTs());
}

class MyTs extends StatelessWidget {
  const MyTs({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => BlocPatternBloc(),
      child: MaterialApp(
        title: 'Flutter',
        theme: ThemeData(primarySwatch: Colors.blue),
        home: Ts(),
      ),
    );
  }
}

class Ts extends StatelessWidget {
  Ts({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<BlocPatternBloc, BlocPatternState>(
      builder: (context, state) {
        List<Task> tasksList = state.allTasks;
        return Scaffold(
          appBar: AppBar(
            title: Text('App task'),
            actions: [
              IconButton(
                  onPressed: () => context.read<BlocPatternBloc>()
                    ..add(AddTaskEvent(
                        task: Task(
                            title: 'title 1', isDelete: false, isDone: false))),
                  icon: Icon(Icons.add)),
              IconButton(
                  onPressed: () => context.read<BlocPatternBloc>()
                    ..add(RemoveTaskEvent(
                        task: Task(
                            title: 'title 1', isDelete: false, isDone: false))),
                  icon: Icon(Icons.delete))
            ],
          ),
          body: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Center(
                child: Chip(label: Text('Tasks:')),
              ),
              Expanded(
                child: ListView.builder(
                    itemCount: tasksList.length,
                    itemBuilder: (context, index) {
                      return ListTile(
                        title: Text(tasksList[index].title),
                        trailing: Checkbox(
                            value: tasksList[index].isDone,
                            onChanged: (value) {
                              context
                                  .read<BlocPatternBloc>()
                                  .add(UpdateTaskEvent(task: 
                                  tasksList[index]));
                            }),
                      );
                    }),
              )
            ],
          ),
          floatingActionButton: FloatingActionButton(
            onPressed: () {},
            tooltip: 'Add task',
          ),
        );
      },
    );
  }
}
